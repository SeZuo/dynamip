//> using dep com.outr::mailgun4s:1.2.3
//> using dep com.outr::scribe:3.13.2
//> using dep com.outr::scribe-file:3.13.2
//> using dep com.lihaoyi::os-lib:0.9.3

package ch.lafabriketo.dinamip

import scribe.Logger
import scribe.Logging
import scribe.file.FileWriter
import scribe.file.string2PathBuilder

/** This script will check public ip and mail the given address on change.
  * It will store the old ip in a file (by default ~/tmp/myip.save)
  * It needs the `dig` bash command to function
  */
object DynamicIP extends Logging {
  @main def app(args: String*): Unit = {
    val logPath = os.home / "tmp"
    if(!os.exists(logPath))
      os.makeDir(logPath)
    val logFile = logPath / "DynamicIP.log"

    
    logger.withHandler(
      writer = FileWriter(logFile.toString)).replace()
    
    //////////////////////////////////////////////////////////////////////////
    // Get my ip
    //////////////////////////////////////////////////////////////////////////

    /* Retrieves current public ip address
     * On ArchLinux you need to install the `bind` package to get that command.
     */
    val digCurrentPublicIp = "dig +short myip.opendns.com @resolver1.opendns.com"


    import sys.process._
    val myIp = digCurrentPublicIp.!!

    println("my ip is " + myIp)

    val ipCachePath = os.home / "tmp"
    val ipCacheFile = ipCachePath / "myip.save"

    if(!os.exists(ipCachePath))
      os.makeDir(ipCachePath)

    val storedIp =
      if(os.exists(ipCacheFile)){
        os.read(ipCacheFile)
      } else {
        "No previous ip stored"
      }
    println("previous ip was " + storedIp)
    
    os.write.over(ipCacheFile, myIp)


    val myIpChangedSinceLastTime =
      myIp != storedIp


      
      
      
    //////////////////////////////////////////////////////////////////////////
    // Send with MailGun
    //////////////////////////////////////////////////////////////////////////
      
    import org.matthicks.mailgun.* 
    import java.io._
    import scala.concurrent._
    import cats.effect.IO
    
    //> using file Config.scala
    import ch.lafabriketo.dinamip.Config.*

    val subject = "Votre serveur à changé d'adresse"

    val mailgun = new Mailgun(domain, apiKey)
    val io: IO[MessageResponse] = mailgun.send(Message(
      from = EmailAddress(mailSender, "Automatic message"),
      to = List(EmailAddress(mailReceiver)),
      subject = subject,
      text = Some(myIp)
    ))

    if(myIpChangedSinceLastTime)
      import cats.effect.unsafe.implicits.global
      io.unsafeRunSync()
      // en : DynamicIp : changed to
      scribe.info("DynamicIp : aliiĝis al " + myIp)
    else
      // en : DynamicIp : script run ended without change
      scribe.info("DynamicIp : Rulado de skripto finis sen ŝanĝo")
  }
}
