# Script Dynamic-IP

  * This script will check public ip and mail the given address on change.
  * It will store the old ip in a file (by default ~/tmp/myip.save)
  * It needs the `dig` bash command to function

# How to compile/deploy/run

Edit `Config.scala` with your MailGun api information.

3 solutions :
    - 1. Locally scala-cli can be used to run directly if installed
    ```scala-cli src/main/scala/DynamicIP.scala```
    - 2. Compile a graalvm native app then run the executable dynamip
    ```scala-cli --power package --native-image src/main/scala/DynamicIP.scala -o dynamip -- --no-fallback --enable-https```
    - 3. sbt can compile or run the app
    ```sbt run```
.
note : Unfortunately mailgun4s is not packaged for native, and 2. does not work
for this version of the script.

# Cron

This script should be run regularly to not loose the connection
One solution is running through a cronjob.

(there is several cron implementations but this works for debian and arch's default)

Use `crontab -e` to edit the cron file
This is for once a month launch of scala app

17 4 1 * * scala-cli /[absolute path to app]/DynamicIP.scala