scalaVersion := "3.4.1"

// set to Debug for compilation details (Info is default)
logLevel := Level.Info


////////////////////////////////////////////////////////////////////////////////
// START OF SCALA NATIVE SECTION

// note : scala native plugin does not work with mailgun4s, but does with
// original script lib smtp implementation

// enablePlugins(GraalVMNativeImagePlugin)
// graalVMNativeImageOptions ++= Seq(
//     // "--allow-incomplete-classpath",
//     "--no-fallback",
//     "--enable-https",
//     // "--enable-http"
// )
// enablePlugins(ScalaNativePlugin)
//
//
// // import to add Scala Native options
// import scala.scalanative.build._
// // defaults set with common options shown
// nativeConfig ~= { c =>
//   c.withLTO(LTO.none) // thin
//     .withMode(Mode.debug) // releaseFast
//     .withGC(GC.none) // for such a small script no need to free anything
// }
//
// // Disable [info] in sbt so that the logging lib can
// // show properly
// outputStrategy := Some(StdoutOutput)

// END OF SCALA NATIVE SECTION
////////////////////////////////////////////////////////////////////////////////

libraryDependencies ++= Seq(
  "com.outr" %% "mailgun4s" % "1.2.3",
  "com.outr" %%% "scribe" % "3.13.2",
  "com.outr" %%% "scribe-file" % "3.13.2",
  "com.lihaoyi" %%% "os-lib" % "0.9.3" 
)

